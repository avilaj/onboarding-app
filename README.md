# Onboarding App

This project took me about 5 hours, I didn't do it on just one session. I've worked it on spare times, bit by bit.
Finished the features on the night and next morning, with some fresh ideas I've added a few last touches.

## Why?
 - /logic has all our business logic modules
 - /core has tools to help us build our logic
 - Ive choosen to use a Bloc pattern style for managin the business logic. Each unit separated by Concept (User/Todos/Current)
 - The Current module is not included in Users as it does a different thing than showing a list of users, and on top of that it is a global shared state. Im not using context or other global state for sharing the current user as the route path its already doing it.
 - I use immer, to ease the update of props in highly nested objects in an immutable way.
 - I test first the business logic and their rules, then i test components and visuals. Most complex scenarios are often on the business rules.
 - Components should have a fairly small interface, and they shouldn't know much about the how tos.
 - I use a Specific schema to handle states, to clearly identify when the module is ready to be consumed. And to ease the decisions of what component to show in the ui.

## Things to improve
 - Ui, add some nice design to it
 - Remove the hardcoded variables (use env variables)
 - Logic is tested, we should add tests for the components aswell
 - Hide or Highlight, users which are completely onboarded from the users list
 - Notify how many pending tasks each user has.
 - Add support for paginating results
 - Dynamically detect where to be redirected (maybe perform a query of initial app configuration and data)
 - Rename Todo to Task, to use the same language as the business
 - Only display loader when its taking too long to load.

## The code
Deployed at: https://onboarding-app-one.vercel.app/
How To build: `npm run build`
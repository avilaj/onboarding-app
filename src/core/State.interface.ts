interface Loading {
    kind: 'Loading';
  }
  
  interface Error {
    kind: 'Error';
    reason: string;
  }
  
  interface Ready<T> {
    kind: 'Ready';
    data: T
  }
  
export type AsyncState<T> = Loading | Error | Ready<T>
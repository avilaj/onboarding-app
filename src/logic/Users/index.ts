import { useUsers as _useUsers } from './useUsers';
import { UsersFetcher } from './Users.repo';

export * from './interfaces'

// just adds the dependencies of the hook
export const useUsers = () => {
    return _useUsers({ UsersFetcher })
};

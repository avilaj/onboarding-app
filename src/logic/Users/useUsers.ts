import { useState, useEffect } from 'react';
import { UsersState, UserFetcherFunction } from './interfaces';

interface HookDependencies {
  UsersFetcher: UserFetcherFunction;
}

export const useUsers = ({ UsersFetcher }: HookDependencies) => {
  const [state, setState] = useState<UsersState>({ kind: 'Loading' });

  useEffect(() => {
    let isMounted = true
    setState({ kind: 'Loading' });
    UsersFetcher().then(result => {
      if (!isMounted) return;
      switch (result.kind) {
        case 'Error':
          setState({ kind: 'Error', reason: 'There was an error.' });
          break;
        case 'Success':
          setState({ kind: 'Ready', data: result.data });
          break;
      }
    })
    return () => {
      isMounted = false
    }
  }, [])


  return {
    state
  };
};

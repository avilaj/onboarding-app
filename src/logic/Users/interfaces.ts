import { ApiResponse } from "core/ApiResponse.interface";
import { AsyncState } from "core/State.interface";

export interface User {
  id: string;
  name: string;
  selected: boolean;
}
export interface Pagination {
  page: number;
  totalPages: number;
  totalResults: number;
}

type UserList = User[];
export type UsersState = AsyncState<UserList>;
export type UsersListFetchResponse = ApiResponse<UserList>;
export type UserFetcherFunction = () => UsersListFetchResponse;

import { renderHook } from '@testing-library/react-hooks'
import {useUsers} from './useUsers'

describe('useUsers', () => {
    const response = {
        kind:'Success',
        data: [
            {id: '1', selected: false},
            {id: '2', selected: false}
        ]
    }
    const UsersFetcher = jest.fn().mockResolvedValue(response)

    it('shows a list of 2 users when its ready', async () => {
        const {result, waitForNextUpdate} = renderHook(() => useUsers({ UsersFetcher }))
        await waitForNextUpdate()

        expect(result.current.state).toHaveProperty('kind', 'Ready')
        expect(result.current.state).toHaveProperty('data.length', 2)
    })

})
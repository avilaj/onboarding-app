import { useRouter } from 'next/router';
export type CurrentUser = ReturnType<typeof useCurrentUser>

export const useCurrentUser = () => {
  const router = useRouter()
  return {
      userId: router.query.user as string,
      onChange: (userId: string) => {
          router.push(`/users/${userId}`)
      }
  }
}
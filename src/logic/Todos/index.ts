export * from './interfaces'
import { useTodos as useTodosBase } from './useTodos'
import { TodoUpdate, TodosFetcher } from './Todos.repo'
import { useCurrentUser } from 'logic/CurrentUser'

// adding the deps
export const useTodos = () => {
    const user = useCurrentUser()
    return useTodosBase({ TodosFetcher, TodoUpdate, userId: user.userId })
}


import {useState, useEffect, useCallback } from 'react'
import { TodosFetcherFunction, TodoUpdateFunction, TodosState, Todo } from './interfaces'

import produce from 'immer'
interface HookDependencies {
  TodosFetcher: TodosFetcherFunction;
  TodoUpdate: TodoUpdateFunction;
  userId: string
}

const initialState: TodosState = {kind: 'Ready', data: []}

const findTodoIndex = (id: string, list: Todo[]) => {
  return list.findIndex(todo => todo.id === id)
}
const toggleTodoById = (id: string, completed: boolean) => produce<TodosState>(draft => {
  if (draft.kind === 'Ready') {
    const index = findTodoIndex(id, draft.data)
    draft.data[index].completed = completed
  }
})

const isTodoCompleted = (id: string, state: TodosState) => {
  let completed = false
  if (state.kind === 'Ready') {
    const index = findTodoIndex(id, state.data)
    completed = state.data[index].completed
  }
  return completed
}

export const useTodos = ({ TodosFetcher, TodoUpdate, userId }: HookDependencies) => {
  const [state, setState] = useState<TodosState>(initialState)
  useEffect(() => {
    if (!userId) return;
    let isMounted =  true;
    setState({ kind: 'Loading' });
    TodosFetcher(userId).then(results => {
      if (!isMounted) return;
      switch (results.kind) {
        case 'Error':
          //@todo: send some logs
          setState({ kind: 'Error', reason: results.reason });
          break;
        case 'Success':
          setState({ kind: 'Ready', data: results.data })
          break;
      }
    })
    return () => { isMounted =  false }
  }, [userId])

  const onComplete = useCallback(async (todoId: string) => {
    if (isTodoCompleted(todoId, state)) return; // do nothing when its completed
    
    setState(toggleTodoById(todoId, true))
 
    const result = await TodoUpdate(todoId, true)

    switch (result.kind) {
      case 'Error':
        setState(toggleTodoById(todoId, false))
        // @todo we can show a notification, a toast
        break;
      case 'Success':
        break;
    }
  }, [state])

  return {
    state,
    actions: { onComplete }
  }
}
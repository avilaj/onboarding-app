import { ApiResponse } from 'core/ApiResponse.interface';
import { AsyncState } from 'core/State.interface'

export interface Todo {
  id: string;
  title: string;
  completed: boolean;
}

type TodoList = Todo[]

export type TodosState = AsyncState<TodoList>;

type ToggleTodoResponse = ApiResponse<Todo>
type TodosListFetchResponse = ApiResponse<TodoList>;

export type TodosFetcherFunction = (userId: string) => TodosListFetchResponse;
export type TodoUpdateFunction = (todoId: string, completed: boolean) => ToggleTodoResponse
import { renderHook, act } from '@testing-library/react-hooks'
import {useTodos} from './useTodos'

describe('useTodo', () => {
    it('fetches todos when user changes', () => {
        const TodoUpdate = jest.fn()
        const TodosFetcher = jest.fn().mockResolvedValueOnce({kind: 'Success'})
        let userId = '1'
        const {rerender, result} = renderHook(() => useTodos({ TodoUpdate, TodosFetcher, userId }))

        expect(TodosFetcher).toBeCalledWith('1')

        userId = '2'
        rerender()

        expect(TodosFetcher).toBeCalledWith('2')
    })

    it('updates a todo when todo is not completed', async () => {
        const TodoUpdate = jest.fn().mockResolvedValueOnce({id: 'a', completed: true})
        const TodosFetcher = jest.fn().mockResolvedValueOnce({kind: 'Success', data: [{id :'a', completed: false}]})
        let userId = '1'

        const { result, waitForNextUpdate } = renderHook(() => useTodos({ TodoUpdate, TodosFetcher, userId }))
        await waitForNextUpdate()
        await act(() => result.current.actions.onComplete('a'));
  
        expect(TodoUpdate).toHaveBeenCalledWith('a', true)
    })


    it('doesnt update a todo when todo is completed', async () => {
        const TodoUpdate = jest.fn().mockResolvedValueOnce({id: 'a', completed: true})
        const TodosFetcher = jest.fn().mockResolvedValueOnce({kind: 'Success', data: [{id :'a', completed: true}]})
        let userId = '1'

        const { result, waitForNextUpdate } = renderHook(() => useTodos({ TodoUpdate, TodosFetcher, userId }))
        await waitForNextUpdate()
        await act(() => result.current.actions.onComplete('a'));
  
        expect(TodoUpdate).not.toHaveBeenCalled()
    })
})
import { request } from 'core/Request';
import {TodosFetcherFunction, TodoUpdateFunction, Todo} from './interfaces'

// @todo: move to env variables
const TODOS_API = 'https://jsonplaceholder.typicode.com/todos';

export const TodosFetcher: TodosFetcherFunction = async (userId: string) => {
    // @todo: handle more error cases
    // @todo handle abort request
    try {
        const response = await request.get<Todo[]>(`${TODOS_API}?userId=${userId}`)
        const data = response.data
        return { kind: 'Success', data }
    } catch (err) {
        return { kind: 'Error', code: err.code, reason: err.message }
    }
}

export const TodoUpdate: TodoUpdateFunction = async (id: string, completed: boolean) => {
    try {
        const response = await request.put<Todo>(`${TODOS_API}/${id}`,  { completed } )
        return { kind: 'Success', data: response.data }
    } catch (err) {
        return { kind: 'Error', code: err.code, reason: err.message }
    }
}

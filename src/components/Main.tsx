import React from 'react';
import TodoList from './TodoList';
import UserList from './UserList';


const Main = () => {
  return <div>
    <h1>Onboarding Tracker</h1>
  <div style={{display: 'grid', gridTemplateColumns: '200px auto'}}>
    <div>
      <h3>Users</h3>
      <UserList />
    </div>
    <div>
      <h3>Tasks</h3>
      <TodoList />
    </div>
  </div>
  </div>
};

export default Main;

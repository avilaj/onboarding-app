import React, {FC} from 'react'
import {MdOutlineCircle, MdCheckCircleOutline} from 'react-icons/md'
import styled from 'styled-components'

const BaseTodo = styled.div`
  font-family: sans-serif;
  padding: .5rem;
  cursor: pointer;
  display: grid;
  grid-template-columns: min-content auto;
  grid-gap: .5rem;
  align-items: center;

`
const CompletedTodoWrapper = styled(BaseTodo)`
  text-decoration: 'line-through'; 
  color: #CCC;
`

const UnfinishedTodoWrapper = styled(BaseTodo)`
  font-weight: bold;
`

export const CompletedTodo = ({children, ...props}) => <CompletedTodoWrapper {...props}><MdCheckCircleOutline /><span>{children}</span></CompletedTodoWrapper>
export const UnfinishedTodo = ({children, ...props}) => <UnfinishedTodoWrapper {...props}><MdOutlineCircle /><span>{children}</span></UnfinishedTodoWrapper>
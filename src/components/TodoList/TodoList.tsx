import React , {FC} from 'react'
import Todo from './Todo'
import {useTodos} from 'logic/Todos'

const TodoList: FC = () => {
    const todosModule = useTodos()
    switch (todosModule.state.kind) {
        case 'Loading':
            return <div>Cargando ...</div>
        case 'Error':
            return <div><strong>Error:</strong>{todosModule.state.reason}</div>
        case 'Ready':
            return <div>
                    {todosModule.state.data.map((todo) => <Todo todo={todo} key={todo.id} onClick={() => todosModule.actions.onComplete(todo.id)} />)}
            </div>
    }
}
export default TodoList
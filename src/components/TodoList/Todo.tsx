import React, {FC} from 'react';
import { Todo as iTodo } from 'logic/Todos'
import {CompletedTodo, UnfinishedTodo} from './Todo.styles'

interface TodoProps {
    todo: iTodo;
    onClick: () => void;
}

const Todo:FC<TodoProps> = ({todo, onClick}) => {
  if (todo.completed) {
    return <CompletedTodo>{todo.title}</CompletedTodo>
  }
  return <UnfinishedTodo onClick={onClick}>
    {todo.title}
  </UnfinishedTodo>
}

export default Todo
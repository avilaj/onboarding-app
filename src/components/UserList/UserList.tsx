import React, {FC} from 'react'
import User from './User'
import { useUsers } from 'logic/Users';
import {useCurrentUser} from 'logic/CurrentUser'
import {UserMenu} from './User.styles'

const UserList: FC = () => {
    const usersModule = useUsers()
    const current = useCurrentUser()

    switch (usersModule.state.kind) {
        case 'Error':
            return <div><strong>Hubo un error cargando los usuarios</strong></div>;
        case 'Loading':
            return <div>Cargando...</div>;
        case 'Ready':
            return <UserMenu>{usersModule.state.data.map(user => <User key={user.id} user={user} isSelected={current.userId == user.id}/>)}</UserMenu>;
    }
}

export default UserList
import styled from 'styled-components'

const BaseUser = styled.a`
    font-family: sans-serif;
    padding: .5rem;
    color: #000;
    text-decoration: none;
`

export const SelectedUser = styled(BaseUser)`
    color: blue;
    font-weight: bold;
`
export const RegularUser = styled(BaseUser)``

export const UserMenu = styled.div`
    display: grid;
    grid-template-columns: 1fr;
`
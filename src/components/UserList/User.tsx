import Link from 'next/link';
import React, {FC} from 'react';
import {User} from 'logic/Users'
import {RegularUser, SelectedUser} from './User.styles'

const User:FC<{user: User, isSelected: boolean}> = ({user, isSelected }) => {
  if (isSelected) return <SelectedUser>{user.name}</SelectedUser>
  return <Link href={`/users/${user.id}`} as={`/users/${user.id}`} passHref><RegularUser>{user.name}</RegularUser></Link>
}

export default User
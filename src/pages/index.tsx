import Head from "next/head";
import { useRouter } from "next/router";
import { useEffect } from "react";

export default function Home() {
  const router = useRouter();
  // @todo remove this, and detect the first user of the list
  useEffect(() => {
    router.replace("/users/1");
  }, [router]);

  return (
    <div>
      <Head>
        <title>Onboarding App</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
    </div>
  );
}
